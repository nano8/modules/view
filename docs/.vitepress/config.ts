import { defineConfig } from 'vitepress';

export default defineConfig({
    title:         'nano/modules/view',
    titleTemplate: 'documentation',
    description:   'nano/modules/view | todo',
    base:          '/modules/view/',
    themeConfig:   {
        search: {
            provider: 'local',
        },
        nav:    [],
        sidebar: [],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/modules/view',
            },
        ],
        editLink: {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/modules/view/-/edit/dev/docs/:path',
        },
    },
});
