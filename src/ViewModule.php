<?php

namespace laylatichy\nano\modules\view;

use laylatichy\nano\core\config\Config;
use laylatichy\nano\core\exception\NanoException;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

final class ViewModule implements NanoModule {
    private static View $view;

    public function __construct() {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        $this->init();
    }

    public static function getInstance(): View {
        if (!isset(self::$view)) {
            throw new NanoException('view module not initialized', HttpCode::INTERNAL_SERVER_ERROR->code());
        }

        return self::$view;
    }

    private function init(): void {
        if (!isset(self::$view)) {
            self::$view = new View(Config::getInstance());
        }
    }
}
