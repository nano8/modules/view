<?php

namespace laylatichy\nano\modules\view;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
final class Template {
    public function __construct(public string $file) {
        // nothing to do here
    }
}