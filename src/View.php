<?php

namespace laylatichy\nano\modules\view;

use laylatichy\nano\core\config\Config;
use laylatichy\nano\core\exception\NanoException;
use laylatichy\nano\core\httpcode\HttpCode;
use ReflectionClass;
use Smarty;

final class View {
    public function __construct(
        private readonly Config $config,
        private readonly Smarty $smarty = new Smarty()
    ) {
        $this->smarty->caching = Smarty::CACHING_OFF;
        $this->smarty->setCompileDir($this->config::getRoot() . '/../.smarty/compiled/');
        $this->smarty->setCacheDir($this->config::getRoot() . '/../.smarty/cache/');
        $this->smarty->setTemplateDir($this->config::getRoot() . '/../views/');
    }

    public function clear(): void {
        array_map(
            'unlink',
            array_filter((array)glob(pattern: $this->config::getRoot() . '/../.smarty/compiled/*'))
        );
        array_map(
            'unlink',
            array_filter((array)glob(pattern: $this->config::getRoot() . '/../.smarty/cache/*'))
        );
    }

    public function assign(array $data): self {
        $this->smarty->assign($data);

        return $this;
    }

    public function fetch(?string $file = null): ?string {
        if (!$file) {
            $file = $this->getFile(debug_backtrace());
        }

        $tpl = $this->smarty->fetch("{$file}.tpl");

        $this->smarty->clearAllAssign();

        return $tpl;
    }

    public function display(?string $file = null): ?string {
        if (!$file) {
            $file = $this->getFile(debug_backtrace());
        }

        $tpl = $this->smarty->display("{$file}.tpl");

        $this->smarty->clearAllAssign();

        return $tpl;
    }

    private function getFile(array $backtrace): string {
        $caller = end($backtrace);

        $class  = new ReflectionClass($caller['class']);
        $method = $class->getMethod($caller['function']);

        $attributes = $class->getAttributes(Template::class);

        if (empty($attributes)) {
            throw new NanoException(
                "no template defined in {$method->class}::{$method->name}",
                HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        if (count($attributes) > 1) {
            throw new NanoException(
                "multiple templates defined in {$method->class}::{$method->name}",
                HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        $template = $attributes[0]->newInstance();

        return $attributes[0]->newInstance()->file . '/' . $method->name;
    }
}
